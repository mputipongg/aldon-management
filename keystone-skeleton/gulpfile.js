var gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	jshintReporter = require('jshint-stylish'),
	livereload = require('gulp-livereload'),
	plumber = require( 'gulp-plumber'),
	nodemon = require('gulp-nodemon'),
	sass = require('gulp-sass'),

	watch = require('gulp-watch'),
	shell = require('gulp-shell'),
	watchify = require('watchify'),
    browserify = require('browserify'),
	resolutions = require('browserify-resolutions'),
	source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    glob = require('glob'),
	gutil = require('gulp-util'),
	sourcemaps = require('gulp-sourcemaps'),
	assign = require('lodash.assign'),
	debug = require('gulp-debug'),
	$ = require('gulp-load-plugins')();


var paths = {
	'src':['./app/models/**/*.js','./app/routes/**/*.js', 'keystone.js', 'package.json'],
	'style': {
		all: './app/styles/**/*.scss',
		output: './dist/styles/'
	}
};

// gulp lint
gulp.task('lint', function(){
	gulp.src(paths.src)
		.pipe(jshint())
		.pipe(jshint.reporter(jshintReporter));
});

// gulp watcher for lint
gulp.task('watch:lint', function () {
	gulp.watch(paths.src, ['lint']);
});

// gulp sass
gulp.task('sass', function(){
	gulp.src( './app/styles/**/*.scss')
		.pipe( plumber() )
		.pipe( sass().on('error', sass.logError ) )
		.pipe( gulp.dest( './dist/styles' ) );
});

// gulp watcher for sass
gulp.task('watch:sass', function () {
	gulp.watch(paths.style.all, ['sass']);
});

gulp.task('watch', [ 'watch:sass','watch:lint' ]);

gulp.task('develop', function () {
    //var uglyLevel = debug ? true : false;

    livereload.listen();
    nodemon({
        script: 'keystone.js',
        ext: 'js hbs',
        stdout: false,
		env: { 'NODE_ENV': 'development' }
    }).on('readable', function () {
        this.stdout.on('data', function (chunk) {
            if (/^Express server listening on port/.test(chunk)) {
                livereload.changed(__dirname);
            }
        });
        this.stdout.pipe(process.stdout);
        this.stderr.pipe(process.stderr);
    });
});

//create array of app js files
var jsFiles = glob.sync('./app/js/*.js');

// add custom browserify options here
var customOpts = {
    entries: jsFiles,
    debug: true
},
opts = assign({}, watchify.args, customOpts),
b = watchify(browserify(opts).plugin( resolutions, [ 'jquery', 'bootstrap' ] ) );

// add transformations here
// i.e. b.transform(coffeeify);
function bundle() {
    return b.bundle()
        // log errors if they happen
        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
        .pipe(source('bundle.js'))
        // optional, remove if you don't need to buffer file contents
        .pipe(buffer())
        // optional, remove if you dont want sourcemaps
        .pipe(sourcemaps.init({ loadMaps: true })) // loads map from browserify file
        // Add transformation tasks to the pipeline here.
        .pipe(sourcemaps.write('./')) // writes .map file
        .pipe(gulp.dest('./dist/js'));
}

gulp.task('js', bundle); // so you can run `gulp js` to build the file
b.on('update', bundle); // on any dep update, runs the bundler
b.on('log', gutil.log); // output build logs to terminal

// ****************************************************************************
// Images
// ****************************************************************************
gulp.task('images', function () {
    return gulp.src('./app/images/**/*')
        .pipe(gulp.dest('./dist/images'));
});

// ****************************************************************************
// Fonts
// ****************************************************************************
gulp.task('fonts', function () {
    return gulp.src( './app/fonts/**/*' )
        .pipe(gulp.dest( './dist/fonts' ) );
} );

// ****************************************************************************
// Clean
// ****************************************************************************
gulp.task( 'clean', require( 'del' ).bind( null, [ 'dist' ] ) );

gulp.task( 'clear', function( done ) {
    return $.cache.clearAll( done );
} );

// ****************************************************************************
// Debug
// ****************************************************************************
gulp.task( 'debug', function() {
    debug = true;
    gutil.log( gutil.colors.green( 'RUNNING IN DEBUG MODE' ) );
    gulp.start( 'default' );
} );

// ****************************************************************************
// Build
// ****************************************************************************
gulp.task( 'build', [ 'sass', 'images', 'fonts' ] );

// ****************************************************************************
// Default
// ****************************************************************************
gulp.task( 'default', [ 'clean' ], function() {
    gulp.start( 'run' );
} );

gulp.task( 'run', [ 'build', 'js', 'develop', 'watch' ] );
